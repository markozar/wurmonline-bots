from sikuli import *
import config
reload(config)
import random
import math

# Counts how many time the stamina bar was refilled
# it gives an idea of how much work has be done. Used for restoration
cycles_counter = 0

class Bot():
  def __init__(self):
    self.craft_cycles = 0
    self.craft_time = 0
    self.soft_exit = False
    # Options
    self.craft_time_margin = config.CRAFT_TIME_MARGIN # If the cycle is this much faster, then too_fast is invoked
    self.restoration = config.RESTORATION # True or False if you want to search for water and meals and restore
    self.max_cycles = config.MAX_CYCLES   # False to wait is_done termination, or a number to cycle only n times
    self.auto_sleep = True    # True if you want the SB to be turned off automatically
    self.wait_stamina = True  # False if you do not need to wait full stamina for each cycle
    #Stats
    self.total_craft_time = 0
    self.craft_times = []
  def before_do(self):
    pass
  def do(self):
    pass
  def after_do(self):
    pass
  def after_full_stamina(self):
    pass
  def is_done(self):
    return False
  def too_fast(self):
    print("Too fast action!")
    return True  # True => exit bot, False => continue botting
  def done(self):
    print "Done!"
    popup("Done!")
    exit()
  def stats(self):
    print "Executed", self.craft_cycles, "crafting cycles"
    print "First craft done in", humanize_time(self.craft_time)
    if(self.craft_cycles > 0):
      self.craft_times.sort()
      c = self.craft_cycles >> 1
      mean = self.craft_times[c]
      if self.craft_cycles % 2 == 0: mean = (mean + self.craft_times[c-1]) / 2.0
      i = 1 if(self.craft_cycles > 1) else 0
      min = humanize_time(self.craft_times[i])
      sigma2 = reduce(lambda s,t: s+((t-mean)**2), self.craft_times)
      dev = math.sqrt(sigma2 / self.craft_cycles)
      mean = humanize_time(mean)
      max = humanize_time(self.craft_times[self.craft_cycles - 1])
      print "Craft times [min, mean, max]: ", min, '-', mean, '-', max
      print "Dev: %.2fs" % dev 
    print "Total crafting time (m:s):", humanize_time(self.total_craft_time)

# Wurm bot: executes a Bot
def execute(bot):
  margin = bot.craft_time_margin
  max = bot.max_cycles
  all_done = False
      
  while not (all_done or bot.is_done() or bot.soft_exit):
    bot.before_do()
    timer_start = time.clock()
    bot.craft_cycles += 1
    bot.do()
    bot.after_do()
    if bot.wait_stamina: wait_full_stamina()
    bot.after_full_stamina()
    elapsed = time.clock() - timer_start
    bot.total_craft_time += elapsed
    bot.craft_times.append(elapsed)
    if not bot.craft_time: bot.craft_time = elapsed
    if margin and (elapsed < bot.craft_time - margin):
      if bot.too_fast():
        break
    if bot.restoration: restoration()
    if(max): all_done = (bot.craft_cycles >= max)
  if bot.soft_exit: print "User terminated"
  if bot.auto_sleep: auto_sleep_off()
  bot.stats()
  bot.done()

# Switch to Wurm Online
def switchTo():
  mouse_pos = Env.getMouseLocation().offset(1,1)
  if config.SWITCH_WURM:   
    switchApp(config.SWITCH_NAME)
    # focus active area hovering a bit on it
    hover(mouse_pos)
  return mouse_pos

# No params return the toolbelt area, with n > 0 returns tool n area in toolbelt
def toolbelt(n=0):
  if n == 0:
    return config.TOOLBELT
  elif n == 1:
    return config.TOOLBELT1
  elif n == 2:
    return config.TOOLBELT2
  elif n == 3:
    return config.TOOLBELT3
  elif n == 4:
    return config.TOOLBELT4
  elif n == 5:
    return config.TOOLBELT5
  elif n == 6:
    return config.TOOLBELT6
  
def event_area():
  return config.EVENTS

def last_event_area():
  return config.LAST_EVENT

def stats_area():
  return config.STATS

# Returns the area containing both crafting slots
def crafting_slots():
  o = config.CRAFTING_SLOTS
  c = crafting_window()
  return c.offset(-c.w,0).below(o[0]).right(o[1])

# Returns only right crafting slot area
def right_crafting_slot():
  o = config.CRAFTING_SLOT_R
  return crafting_window().offset(o[0],o[1]).below(o[2]).right(o[3])

# Returns only right crafting slot area
def left_crafting_slot():
  o = config.CRAFTING_SLOT_L
  return crafting_window().offset(o[0],o[1]).below(o[2]).grow(5)

def chat_area():
  return config.CHAT

# Windows ====================================
_inventoryR = 0
def inventory():
  global _inventoryR
  if not _inventoryR:
    _inventoryR = exists("windows/inventory.png")
    if(_inventoryR):
      # takes the area of interest where you can find things in inventory
      _inventoryR = _inventoryR.offset(50,50).below().grow(50)
  return _inventoryR

_forgeR = 0
def forge():
  global _forgeR
  if not _forgeR:
    _forgeR = exists("windows/forge.png")
    if(_forgeR):
      # takes the area of interest where you can find things in inventory
      _forgeR = _forgeR.offset(50,50).below().grow(50)
  return _forgeR

_craftingW = 0
def crafting_window():
  global _craftingW
  if not _craftingW:
    _craftingW = find("windows/crafting.png")
  return _craftingW

# Patterns ===================================
def full_stamina():
  return Pattern("patterns/full_stamina.png").exact()

def inventory_water():
  return Pattern("resources/water.png").similar(0.94)

# Return an image file name of the tool that is used in the LEFT crafting slot
# can be used to find the tool in the toolbelt and repair it
def get_crafting_tool():
  c = left_crafting_slot()
  r = Region(c.x + 32, c.y + 28, 29, 29)
  f = capture(r)
  tool = toolbelt().exists(f, 1)  
  os.remove(f)
  return tool

# Buttons ====================================
_create_btn = 0
def click_create():
  global _create_btn
  if not _create_btn:
    _create_btn = find("buttons/create.png")
  rnd_click(_create_btn)

_continue_btn = 0
def click_continue():
  global _continue_btn
  if not _continue_btn:
    _continue_btn = find("buttons/continue.png")
  rnd_click(_continue_btn)

# Actions ====================================
_stamina_full = False
def wait_full_stamina():
  global cycles_counter, _stamina_full
  stats_area().wait(full_stamina(), FOREVER)

#  is_full = stats_area().exists(full_stamina(), 0.3)
#  while not is_full:
#    wait(0.5)
#    is_full = stats_area().exists(full_stamina(), 0.3)

#  observeStamina(full_stamina())
#  while not _stamina_full:
#    wait(1)
#    paste("q")
#  _stamina_full = False
  cycles_counter += 1

def _filled():
  global _stamina_full
  print "Stamina is full!"
  _stamina_full = True
  stats_area().stopObserver() 
  
def observeStamina(full):
  stats_area().onAppear(full, _filled)
  stats_area().observe()
  
def click_menu(pos, action, simil = 0.80, timeout = FOREVER):
  pos.rightClick()
  wait(random.uniform(0.3, 0.5))
  o = config.MENU
  menu = pos.offset(o[0],o[1]).below(o[2]).grow(o[3])
  action = menu.wait(Pattern(action).similar(simil), timeout)
  if action: rnd_click(action)
  return action

def rnd_click(area):
  click(randomize_pos(area))

def rnd_hover(area):
  hover(randomize_pos(area))

def select_max_items():
  wait("windows/removing_items.png", FOREVER)
  #click("buttons/send.png")
  type(Key.ENTER)

def select_n_items(n):
  wait("windows/removing_items.png", FOREVER)
  type(str(n))
  #click("buttons/send.png")
  type(Key.ENTER)

def auto_sleep_off():
  type(Key.ENTER)
  wait(0.5)
  type("&sleep")
  type(Key.ENTER)
  wait(1)
  sb = event_area().wait("events/sleep_bonus.png", FOREVER)
  sb_off = event_area().exists("events/sleep_bonus_off.png", 1)
  while not sb_off:
    type(Key.ENTER)
    wait(0.5)
    type("&fsleep")
    type(Key.ENTER)
    wait(1)
    sb_off = event_area().exists("events/refrain_from_using_sb.png", 1)
    if not sb_off:
      wait(15)
    else:
      print "Sleep bonus deactivated"

_right_slot = 0
def is_right_crafting_slot_empty():
  global _right_slot
  if not _right_slot:
    _right_slot = right_crafting_slot()
  return _right_slot.exists(Pattern("locations/empty_right_craft_slot.png").similar(0.97), 0.8)

_left_slot = 0
def is_left_crafting_slot_empty():
  global _left_slot
  if not _left_slot:
    _left_slot = left_crafting_slot()
  return _left_slot.exists(Pattern("locations/empty_right_craft_slot.png").similar(0.97), 0.8)

_all_slots = 0
def is_any_crafting_slot_empty():
  global _all_slots
  if not _all_slots:
    _all_slots = crafting_slots()
  return _all_slots.exists(Pattern("locations/empty_right_craft_slot.png").similar(0.97), 0.8)


#Drinks if water available on screen
def drink():
  cursor_pos = Env.getMouseLocation()
  water = exists(inventory_water())
  if(water):
    click_menu(water, "menus/drink.png", 0.8, 3)
    hover(cursor_pos)

def eat():
  #TODO
  pass

# Do a drink and an eat after RESTORE_EVERY cycles, if water or meal available
def restoration():
  global cycles_counter
  if(cycles_counter >= config.RESTORE_EVERY):
    cycles_counter = 0
    try:
      drink()
      eat()
    except FindFailed:
      print "No restoration available"

# tool is a region
def repair(tool):
  rnd_hover(tool)
  wait(random.uniform(0.2, 1))
  type(config.KEY_REPAIR)

# Shared ====================================
def randomize_pos(area):
  margin_x = margin_y = 5
  pos_x = area.x + (area.w / 2)
  pos_y = area.y + (area.h / 2) 
  if(area.w > (2 * margin_x + 3)):
    pos_x = random.randint(area.x + margin_x, area.x + area.w - margin_x)
  if(area.h > (2 * margin_y + 3)):
    pos_y = random.randint(area.y + margin_y, area.y + area.h - margin_y)
  return Location(pos_x, pos_y)

def humanize_time(secs):
  mins, secs = divmod(secs, 60)
  #hours, mins = divmod(mins, 60)
  return '%02d:%02ds' % (mins, secs)