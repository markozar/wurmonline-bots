# Put rock shard in slot 2 and stone chisel in slot 3
# examine the target before starting the scritp
import config
reload(config)
import wurm
reload(wurm)
import random

# CUSTOMS ================================#
# tries to make this many improvements before waiting for stamina,
# but stamina usually goes up faster
DEPTH = 4
#=========================================#


class ImpMasonry(wurm.Bot):    
  def __init__(self):
    wurm.Bot.__init__(self)
    self.craft_time_margin = False
    self.auto_sleep = False

    wurm.switchTo()
    wait(2)
    pos = Env.getMouseLocation()
    self.target = Region(pos.x, pos.y, 10, 10)
    self.with_rocks = wurm.event_area().exists("events/with_a_rock_shard.png")
    self.chisel = wurm.toolbelt().find(Pattern("tools/active_stone_chisel.png").similar(0.88))
    self.chisel_used = 0
    self.craft_bar = Region(732,769,103,32)
    self.repair_randomizer = 1
    self.stop = False
  
  def do(self):
    for cycle in range(DEPTH):
      self.do_cycle()

  def do_cycle(self):
    menu = action = 0
    if self.with_rocks:
      type(str(2))
      menu = "menus/improve.png"
    else:    
      type(str(3))
      menu = "menus/chip.png"
      self.chisel_used += 1   
    #wait(random.uniform(0.3, 0.5))
    action = wurm.click_menu(self.target, menu, 0.8, 10)
    if not action: self.stop = True
    wait(5)
    self.craft_bar.wait(Pattern("patterns/not_crafting.png").similar(0.80), FOREVER)
    self.prepare()

  # Do actions before the stamina bar fills, so we can prepare next action in advance
  def prepare(self):
    if wurm.event_area().exists("events/you_damage_the.png"):
      type(config.KEY_REPAIR)
    else:
      with_rocks = wurm.event_area().exists("events/with_a_rock_shard.png", 0.2)
      with_chisel = wurm.event_area().exists("events/with_a_stone_chisel.png", 0.2)
      if(not with_rocks) and (not with_chisel):
        self.stop = True
      elif not with_chisel:
        self.with_rocks = True
      elif not with_rocks:
        self.with_rocks = False
      else:
        self.with_rocks = (with_rocks.y > with_chisel.y)
          
    if(self.chisel_used % (9 + self.repair_randomizer) == 0):
      wurm.repair(self.chisel)
      self.repair_randomizer = random.randint(1, 7)

  def is_done(self):
    return self.stop

  def done(self):
    wurm.repair(self.chisel)
    wurm.Bot.done(self)
  
wurm.execute(ImpMasonry())
