# The forge must be filled with cauldrons and pans inside.
# Expand everything in the forge then close just single cauldrons, leaving cauldron tree open.
# Make sure that the last cauldron fits the window when expanded.
# Open inventory, open fsb with veggies. Do not highlight needed veggies in fsb.
# Put stats background behind forge, fsb and inventory windows.

# TODO: consider the possibility that there are not enough veggies.

import config
reload(config)
import wurm
reload(wurm)
import random

# CUSTOMS ================================#
# [veggies entry in container, veggies entries in inventory, pan weight before putting this veggy]
ONIONS = ["veggies/onions.png", "veggies/onion.png", "patterns/2.50.png"]
GARLICS = ["veggies/garlics.png", "veggies/garlic.png", "patterns/2.55.png"] # onion adds 0.05 weight, so pan weight before putting garlic is 2.55
POTATOS = ["veggies/potato.png", "veggies/potato.png", "patterns/2.50.png"]

VEGGY1 = ONIONS   # can be False (but at least one VEGGY must be defined)
VEGGY2 = GARLICS  # can be False (but at least one VEGGY must be defined)
#=========================================#

Settings.DelayAfterDrag = 0.1
Settings.DelayBeforeDrop = 0.3
Settings.MoveMouseDelay = 0.2

class PanFillerBot(wurm.Bot):  
  def __init__(self):
    wurm.Bot.__init__(self)
    wurm.switchTo()
    self.auto_sleep = False
    self.wait_stamina = False
    self.forge = wurm.forge()
    self.inventory_bag = find("locations/inventory_group.png")
    cauldrons = self.forge.findAll(Pattern("objects/+cauldron.png").similar(0.96).targetOffset(-61,1))
    self.cauldrons = sorted(cauldrons, key=lambda t:t.y)
    if VEGGY1: self.veggy1 = find(Pattern(VEGGY1[0]))
    if VEGGY2: self.veggy2 = find(Pattern(VEGGY2[0])) 
    self.filled = False

  def take(self, veggies, qty):
    dragDrop(veggies, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_n_items(qty + 1)
    wait(1)

  def drag_veggies(self, veggies, pans):
    # Opens veggies tree
    root = wurm.inventory().find(veggies[1])
    click(Location(root.x - 11, root.y + 8))  # clicks on [+] to open veggies
    hover(root.offset(0, -30))  # reveals first veggy in the tree, otherwise the cursor hides it
    wait(1)
    # Drags veggies
    w = Pattern(veggies[2]).similar(0.96)
    veggies = wurm.inventory().findAll(Pattern(veggies[1]).similar(0.96))
    veggies = sorted(veggies, key=lambda t:t.y)
    
    target = [veggies[0], veggies[2]]
    wf = pans[0].right().find(w)
    ux = wf.x
    uy = wf.y
    count = len(pans)
    last = count - 1
    wl = pans[last].right().find(w)
    bx = wl.x + wl.w
    by = wl.y + wl.h
    weights = Region(ux, uy, bx - ux, by - uy).grow(2) 

    # drag fast, it alternates among first and third to minimize misses
    for pan in pans:
      if count > 2:
        veggy = target[count % 2]
      else:
        veggy = target[0]
      count -= 1
      if count > 0: dragDrop(veggy, pan) # last pan intentionaly missed
    hover(root) # moves away cursor from pans
    wait(1)

    # complete missed drags (last drag is always intentionally missed to speed up findAll)
    weights = weights.findAll(w)
    for weight in weights:
      dragDrop(veggy, weight.offset(-80,0))
      wait(0.3)
      weight.waitVanish(w)
      hover(root)

    dragDrop(root, self.veggy1) # puts back the ramaining 1 veggy in the fsb

  def fill(self, c):
    click(c)
    hover(Location(c.x, c.y - 25))  # reveals all cauldrons moving cursor away
    pans = findAll(Pattern("objects/_frying_pan.png").similar(0.96))
    pans = sorted(pans, key=lambda t:t.y)
    qty = len(pans)
    
    if VEGGY1:
      self.take(self.veggy1, qty)
      self.drag_veggies(VEGGY1, pans)

    if VEGGY2:
      self.take(self.veggy2, qty)
      self.drag_veggies(VEGGY2, pans)

    click(c)
    wait(1)

  def after_do(self):
    wurm.Bot.after_do(self)

  def do(self):
    for c in self.cauldrons:
      self.fill(c)    
    self.filled = True
    
  def is_done(self):
    return self.filled

  def done(self):
    wurm.Bot.done(self)

wurm.execute(PanFillerBot())
