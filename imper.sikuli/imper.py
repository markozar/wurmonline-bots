from sikuli import *
import config
reload(config)
import wurm
reload(wurm)
import random

class Bot(wurm.Bot):
  imp_icons = []
  menu_icons = []
  to_repair = []

  def __init__(self):
    wurm.Bot.__init__(self)
    self.current_tool = 0 # current tool index in the list
    self.when_repair = random.randint(8, 14)
    self.last_actions = []
     
    wurm.switchTo()
    self.selectImps() # selects area in forge where are tools to imp. All must be repaired before this
    wurm.switchTo()
   
  def selectImps(self):
    self.imp_area = selectRegion("Imping area")
    tools2imp = self.imp_area.findAll(Pattern("imp_icons/0.00.png").similar(0.90))
    tools2imp = sorted(tools2imp, key=lambda t:t.y)
    self.tools = list([t for t in tools2imp])
    self.tools_count = len(self.tools)
    last_tool = self.tools[self.tools_count - 1]
    self.safe_area = last_tool.offset(0, 50)

  def repair_tools(self):
    for t in reversed(self.tools):
      if(not t.grow(1).exists(Pattern("imp_icons/0.00.png").similar(0.94), 0.2)):
        self.last_actions.append("Repairing at y: " + str(t.y))
        wurm.rnd_hover(t)
        wait(0.5)
        type(config.KEY_REPAIR)

  def repair_toolbelt_tools(self, force = False):
    if force or (self.craft_cycles >= self.when_repair):
      for n, repair in enumerate(self.to_repair):
        if repair:
          wurm.repair(wurm.toolbelt(n+1))
          wait(random.uniform(0.2, 1))
      self.when_repair += random.randint(8, 14)

  # Called just before issuing an improve action
  def before_queue(self, n):
    pass

  # Called just after an improve action has been issued
  def after_queue(self, n):
    pass
  
  def imp_tool(self, tool):
    action = 0
    o = config.IMP_ICON
    for n, icon in enumerate(self.imp_icons):
      action = tool.offset(o[0],o[1]).right(o[2]).grow(o[3]).exists(Pattern(icon).similar(0.80), 0.3)
      if(action):
        self.last_actions.append("Imping with: " + icon)
        self.before_queue(n)
        type(str(n+1))
        wait(0.3)
        wurm.click_menu(tool, self.menu_icons[n])
        self.after_queue(n)
        break
    if not action:
      self.last_actions.append("No imp tool found for:")
      f = capture(tool.offset(50,2).right(20).grow(3))
      self.last_actions.append(f) 
    return n

  # Called just after all improve action has been issued
  def after_queue_all(self):
    pass
 
  # For 4 or more tools to imp at the same time
  def imp_many_tools(self):
    for n in range(config.IMP_QUEUE_DEPTH):
      self.imp_tool(self.tools[self.current_tool])
      wait(random.uniform(0.3, 0.8))
      self.current_tool = (self.current_tool + 1) % self.tools_count
      if(self.current_tool == 0): break
    self.after_queue_all() 
    wait(13)
  
  def imp_few_tools(self):
    for n in range(self.tools_count):
      self.imp_tool(self.tools[self.current_tool])
      wait(random.uniform(0.3, 0.8))
      self.current_tool = (self.current_tool + 1) % self.tools_count
    self.after_queue_all() 
    wait(max(13, 5 * self.tools_count))

  def before_do(self):
    wurm.Bot.before_do(self)
    self.last_actions = []
    if(self.current_tool == 0): self.repair_tools()
    self.repair_toolbelt_tools()
  
  def do(self):
    wurm.rnd_hover(self.safe_area)
    if(self.tools_count < config.IMP_QUEUE_DEPTH):
      self.imp_few_tools()
    else:
      self.imp_many_tools()

  def done(self):
    self.repair_toolbelt_tools(True)
    wurm.Bot.done(self)

  def too_fast(self):
    print "Last actions were:"
    print self.last_actions
    return wurm.Bot.too_fast(self)
