# Execution:
# - put a rock shard in the crafting slot and select a brick
# - remove rock shard and start the script
# - when asked select the storage region where rock shards are.
#   Bricks will be dragged there also.
# Enjoy

import wurm
reload(wurm)
import random

###############################################################
MATERIALS = {
    'rock': ["resources/rock_shards.png", "resources/rock_shards.png", "resources/stone_bricks.png"],
    'marble': ["resources/marble_shards.png", "resources/shards_marble.png", "resources/marble_bricks.png"]
    }
USING = 'marble' # [rock | marble]
###############################################################

class Bricker(wurm.Bot):
  inventory = wurm.inventory()
  inventory_bag = find("locations/inventory_group.png")
  crafting_slot = find("locations/empty_right_craft_slot.png")
  chisel_tool = wurm.toolbelt().find("tools/toolbelt_stone_chisel.png")

  def __init__(self):
    wurm.Bot.__init__(self)
    self.craft_time_margin = False
    self.auto_sleep = False
    
    wurm.switchTo()
    self.storage = selectRegion("Select rocks and bricks storage area")
    wurm.switchTo()
    self.combine = self.crafting_slot.find("buttons/combine.png")
    self.have_rocks = True

  def take_rocks(self):
    storage_rocks = self.storage.exists(MATERIALS[USING][0])
    if(not storage_rocks):
      self.have_rocks = False
      return
    dragDrop(storage_rocks, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_max_items()
  
  def craft_cycle(self):
    inventory_rocks = self.inventory.wait(Pattern(MATERIALS[USING][1]).similar(0.80), FOREVER)
    dragDrop(inventory_rocks, self.crafting_slot)
    click(self.combine)
    wait(1)
    for n in range(3):
      wurm.click_create()
      wait(10)
      if(n == 2):
        wurm.repair(self.chisel_tool)
      wait(10)
      wurm.wait_full_stamina()

  def do(self):
    wait(random.uniform(0.2, 1))
    self.take_rocks()
    if self.have_rocks:
      wait(1)
      self.craft_cycle()
      bricks = self.inventory.find(MATERIALS[USING][2])
      dragDrop(bricks, self.storage)

  def is_done(self):
    return not self.have_rocks

  def done(self):
    wurm.repair(self.chisel_tool)
    wurm.Bot.done(self)

wurm.execute(Bricker())