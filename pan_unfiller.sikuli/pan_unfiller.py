# The forge must be filled with cauldrons and pans inside.
# Expand everything in the forge then close just single cauldrons, leaving cauldron tree open.

import config
reload(config)
import wurm
reload(wurm)
import random

# CUSTOMS ================================#
# [meal papttern, True for drag or False to use take, wait time after a cauldorn unfill or False]
STEW = ["meals/stew.png", True, False]
MEAL = ["meals/meal.png", False, 5]
FOOD = STEW
#=========================================#

Settings.DelayAfterDrag = 0.1
Settings.DelayBeforeDrop = 0.3
Settings.MoveMouseDelay = 0.2

class PanUnfillerBot(wurm.Bot):  
  def __init__(self):
    wurm.Bot.__init__(self)
    wurm.switchTo()
    self.auto_sleep = False
    self.wait_stamina = False
    if FOOD[1]: self.container = selectRegion("Select where to empty")
    wurm.switchTo()
    hover(Location(10,10))
    self.forge = wurm.forge()
    cauldrons = self.forge.findAll(Pattern("objects/+cauldron.png").similar(0.96).targetOffset(-61,1))
    self.cauldrons = sorted(cauldrons, key=lambda t:t.y)
    self.emptied = False

  def empty(self, c):
    click(c)
    hover(Location(c.x, c.y - 25))  # reveals all cauldrons moving cursor away
    while(1):
      try:
        meals = self.forge.findAll(FOOD[0])
        meals = sorted(meals, key=lambda t:-t.y)
        for meal in meals:
          if FOOD[1]:
            dragDrop(meal, self.container)
          else:
            hover(meal)
            type(config.KEY_TAKE)
        wait(2)
      except FindFailed:
        break
    click(c)
    if FOOD[2]: wait(FOOD[2])

  def after_do(self):
    wurm.Bot.after_do(self)

  def do(self):
    for c in self.cauldrons:
      self.empty(c)    
    self.emptied = True
    
  def is_done(self):
    return self.emptied

  def done(self):
    wurm.Bot.done(self)

wurm.execute(PanUnfillerBot())
