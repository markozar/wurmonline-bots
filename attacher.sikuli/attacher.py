# Put object to continue in left crafting slot and resource in right slot
import wurm
reload(wurm)

class AttacherBot(wurm.Bot):    
  def __init__(self):
    wurm.Bot.__init__(self)
    self.craft_time_margin = False
    self.auto_sleep = False
    wurm.switchTo()
  
  def do(self):
    wurm.click_continue()
    wait(12)
    
  def is_done(self):
    return wurm.is_right_crafting_slot_empty()

wurm.execute(AttacherBot())
