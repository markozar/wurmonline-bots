import config
reload(config)
import wurm
reload(wurm)
import random

Settings.WaitScanRate = 1

# CUSTOMS ================================#
KNIFE_SLOT = 6
AUTO_SLEEP = False
MEATS_TO_TAKE = 30
REAL_QUEUE = 6
#=========================================#

class Fileter(wurm.Bot):
  inventory_bag = find("locations/inventory_group.png")

  def __init__(self):
    wurm.Bot.__init__(self)
    # configs
    self.auto_sleep = AUTO_SLEEP    # True if you want the SB to be turned off automatically
    self.wait_stamina = False  # False if you do not need to wait full stamina for each cycle

    #custom init    
    wurm.switchTo()
    wait(2)
    all_satchels = findAll(Pattern("objects/_satchel.png").similar(0.96))
    self.satchels = sorted(all_satchels, key=lambda t:t.y)
    self.next_satchel = 0
    self.satchels_count = len(self.satchels)
    self.storage = selectRegion("Select meat storage area")
    wurm.switchTo()
    self.have_meat = True
  
  def repair_knife(self):
    #pos = Env.getMouseLocation() # Updates every time location so I can adjust position
    wurm.repair(wurm.toolbelt(KNIFE_SLOT))
    #hover(pos) 

  def get_meat(self):
    storage_meats = self.storage.exists("resources/cooked_meat.png")
    if(not storage_meats):
      self.have_meat = False
      return
    dragDrop(storage_meats, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_n_items(MEATS_TO_TAKE)
    self.meat = wurm.inventory().exists(Pattern("resources/cooked_meat.png").similar(0.90))
    if(not self.meat):
      dragDrop(storage_meats, self.inventory_bag)
      wait(random.uniform(0.2, 0.4))
      wurm.select_max_items()
    self.meat = wurm.inventory().exists(Pattern("resources/cooked_meat.png").similar(0.90))

  def butcher(self):
    butchering = True
    while butchering:
      wurm.click_menu(self.meat,"menus/filet.png")
      for n in range(REAL_QUEUE):
        filet = wurm.inventory().wait(Pattern("resources/filet.png").similar(0.90), FOREVER)
        dragDrop(filet, self.satchels[self.next_satchel])
        wait(1)
        self.next_satchel = (self.next_satchel + 1) % self.satchels_count
        butchering = wurm.inventory().exists(Pattern("resources/cooked_meat.png").similar(0.90), 1)
        if(not butchering):
          break
    
  def do(self):
    self.get_meat()
    if(self.have_meat):
      self.butcher()
    self.repair_knife()
  
  def is_done(self):
    return not self.have_meat 

wurm.execute(Fileter())
