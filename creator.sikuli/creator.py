# Put tool in left crafting slot and resource in right slot
import wurm
import random
reload(wurm)

class CreatorBot(wurm.Bot):  
  def __init__(self):
    wurm.Bot.__init__(self)
    wurm.switchTo()
    
    self.craft_time_margin = False
    self.auto_sleep = False

    self.tool = wurm.get_crafting_tool()
    self.reapir_at = random.randint(2, 4)

  def after_do(self):
    wurm.Bot.after_do(self)
    if self.tool and (self.craft_cycles >= self.reapir_at):
      self.reapir_at += random.randint(2, 4)
      cursor_pos = Env.getMouseLocation().offset(random.randint(10, 300), random.randint(10, 100))
      wurm.repair(self.tool)
      hover(cursor_pos)

  def do(self):
    wurm.click_create()
    wait(10)
    
  def is_done(self):
    return wurm.is_any_crafting_slot_empty()

  def done(self):
    if self.tool: wurm.repair(self.tool)
    wurm.Bot.done(self)

wurm.execute(CreatorBot())
