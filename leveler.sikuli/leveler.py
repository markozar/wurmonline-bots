# After wurm focuses, you have 2s to put the cursor near the base
# of the tile to level down (this bot does not work to level up).
import config
reload(config)
import wurm
import random
reload(wurm)

# CUSTOMS ================================#
ACTION_TIME = 5   # level action timer
DIRTS = 10        # How many dirts to carry before moving them to the storage
FAST_ACTION = 15  # if action requir
TOOLBELT_POS = 4  # position of the shovel in the toolbelt
#=========================================#

class LevelerBot(wurm.Bot):
  inventory = wurm.inventory()

  def __init__(self):
    wurm.Bot.__init__(self)
    self.craft_time_margin = False
    self.auto_sleep = False
    
    wurm.switchTo()
    wait(2)
    self.level_here = Env.getMouseLocation()
    self.storage = selectRegion("Select dirt storage")
    #self.sands = int(input("How much sand to use?"))
    wurm.switchTo()
    self.craft_bar = Region(942,769,129,23)
    self.stop = False
   
  def move_dirt_to_storage(self):
    dirt = self.inventory.exists(Pattern("resources/dirt.png").similar(0.9))
    if dirt: dragDrop(dirt, self.storage)

  def repair_shovel(self):
    wurm.repair(wurm.toolbelt(TOOLBELT_POS))

  def do(self):
    hover(self.level_here)
    wait(0.2)
    type(config.KEY_LEVEL)
    start = time.clock()
    last = start
    wait(2)
    self.level_here = Env.getMouseLocation() # Allow to correct position
    while self.craft_bar.exists(Pattern("patterns/leveling_action.png")):
      elapsed = time.clock() - last
      trigger = elapsed % (ACTION_TIME * DIRTS)
      if trigger < (elapsed-1):
        last += ACTION_TIME * DIRTS
        self.move_dirt_to_storage()
      wait(1)
    self.move_dirt_to_storage() 
    if (time.clock() - start) < FAST_ACTION:
      self.repair_shovel()
      self.stop = True
      print "Stopping"
        
  def is_done(self):
    return self.stop
  
wurm.execute(LevelerBot())
