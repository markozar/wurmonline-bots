from sikuli import *

setBundlePath('/Users/marco/Documents/sikuli/custom-images')
addImagePath('/Users/marco/Documents/sikuli/images')

# Toolbelt ======================================
TOOLBELT = Region(996,638,274,47)

TOOLBELT1 = Region(1005,650,26,23)
TOOLBELT2 = Region(1046,646,31,27)
TOOLBELT3 = Region(1093,649,29,25)
TOOLBELT4 = Region(1138,649,28,24)
TOOLBELT5 = Region(1182,648,28,24)
TOOLBELT6 = Region(1225,646,28,28)

# Events ========================================
EVENTS = Region(739,692,520,79)
LAST_EVENT = Region(800,727,463,42)

# Statistics ====================================
STATS = Region(535,598,241,103)

# Chat ==========================================
CHAT = Region(6,770,175,21)

# Constants =====================================
RESTORE_EVERY = 10		# Tries to restore water/food every this number of actions (do cycles)
QUEUE_DEPTH = 6				# The character queue depth (update it as it grows)
IMP_QUEUE_DEPTH = 5		# Max tools that can be imped at once before stamina depletes
SWITCH_WURM = True
SWITCH_NAME = "Wurm Online"

# Bot options ===================================
CRAFT_TIME_MARGIN = 10 # in seconds. put to False to ignore fast actions
RESTORATION = True     # True to perform restoration while botting
MAX_CYCLES = False     # or a number. If a number: max number of craft cycles, then bot exits

# Search areas ==================================
MENU = [65,65,65,70]  #offset x, offset y, below, grow
CRAFTING_SLOTS = [70,300] # below, right
CRAFTING_SLOT_R = [100,0,70,100]  # offset x, offset y, below, right
CRAFTING_SLOT_L = [26,0,70] # offset x, offset y, below
IMP_ICON = [50,2,20,3]      # offset x, offset y, right, grow

# Wurm Hotkeys ==================================
KEY_REPAIR = "r"
KEY_MINE = "m"
KEY_DIG = "i"
KEY_TAKE = "t"
KEY_LEVEL = "l"
