# Select are in the forge that contains tools to imp, only select the area containing
# dmg column up to imp icon column
# A glowing iron lump must be in the inventory or in the forge (but bound to first toolbelt slot)
# Tools must be in in toolbelt, in the order given by imp_icons

# BUGS:
# - when we imp only 2 objects, happens that the first one has whetstone icon
#   and the second one remain selected (last to be clicked on). The selection on
#   the secondon object does not allow to recognize whetstone icon in the first one.

import config
reload(config)
import wurm
reload(wurm)
import imper
reload(imper)

###############################################################
LUMPS = {
    'iron': ["imp_icons/iron_lump.png", "resources/iron_lump.png", "resources/iron_lump_named.png"],
    'steel': ["imp_icons/iron_lump.png", "resources/steel_lump.png", "resources/steel_lump_named.png"],
    'lead': ["imp_icons/iron_lump.png", "resources/lead_lump.png"],
    'copper': ["imp_icons/copper_lump.png", "resources/copper_lump.png"],
    'gold': ["imp_icons/gold_lump.png", "resources/gold_lump.png"],
    'silver': ["imp_icons/silver_lump.png", "resources/silver_lump.png"]
    }
USING = 'iron'  # [iron | steel | lead | copper | gold | silver]
NAMED = 0 # 0 for not named, 1 for named lump (Fendir)
LUMP_DROP = 'imp' # [imp | safe] Use safe when imping containers, lump will be dropped underneath them, in the safe area
###############################################################

class BSImper(imper.Bot):
  imp_icons = [LUMPS[USING][0],"imp_icons/water.png","imp_icons/whetstone.png","imp_icons/pelt.png","imp_icons/hammer.png"]
  menu_icons = ["menus/improve.png","menus/temper.png","menus/sharpen.png","menus/polish.png","menus/hammer.png"]
  to_repair =[0, 0, 0, 0, 1]

  def __init__(self):
    imper.Bot.__init__(self)
    self.restoration = True
    #self.max_cycles = 1
    #self.craft_time_margin = 40
    self.craft_time_margin = False
    
    # Windows and locations
    self.inventory = wurm.inventory()
    self.inventory_bag = find("locations/inventory_group.png")
    self.forge = wurm.forge()

    #Variables
    self.lump = LUMPS[USING][1 + NAMED]
    self.lump_in_inventory = self.inventory.exists(self.lump)
    self.lump_used = False
    self.drop_area = (LUMP_DROP == 'imp' and self.imp_area or self.safe_area)
   
  def move_lump_to_forge(self):
    cursor_pos = Env.getMouseLocation()
    lump = self.inventory.find(self.lump)
    dragDrop(lump, self.drop_area)
    self.lump_in_inventory = False
    hover(cursor_pos)
  
  def move_lump_to_inventory(self):
    lump = self.forge.find(self.lump)
    dragDrop(lump, self.inventory_bag)
    self.inventory.wait(Pattern(self.lump), FOREVER)
    self.lump_in_inventory = True

  def before_do(self):
    imper.Bot.before_do(self)
    self.lump_used = False

  def before_queue(self, n):
    if(n == 0):
      self.lump_used = True
      if not self.lump_in_inventory: self.move_lump_to_inventory()
 
  def after_queue_all(self):
    if self.lump_in_inventory and not self.lump_used: self.move_lump_to_forge()
    

wurm.execute(BSImper())












  