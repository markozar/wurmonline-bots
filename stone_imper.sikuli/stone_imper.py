# Select are in the forge that contains tools to imp, only select the area containing
# dmg column up to imp icon column
# A glowing iron lump must be in the inventory or in the forge (but bound to first toolbelt slot)
# Tools must be in in toolbelt, in the order given by imp_icons

import wurm
import imper
reload(imper)

class StoneImper(imper.Bot):
  imp_icons = ["", "imp_icons/rock_shard.png", "imp_icons/stone_chisel.png"]
  menu_icons = ["", "menus/improve.png", "menus/chip.png"]
  to_repair = [0, 0, 1, 0, 0]

  def __init__(self):
    imper.Bot.__init__(self)
    #self.restoration = False
    #self.max_cycles = 1
    #self.craft_time_margin = 20
    self.craft_time_margin = False

wurm.execute(StoneImper())
