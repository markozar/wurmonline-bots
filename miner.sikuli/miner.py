import config
reload(config)
import wurm
reload(wurm)
import random

Settings.WaitScanRate = 1

# CUSTOMS ================================#
SKILLING = 0  # [0|1]
SKILLING_WALL_CYCLES = 4
SEE_CEILING = 0
#=========================================#

class Miner(wurm.Bot):
  mine_cycles = config.QUEUE_DEPTH
  ceiling = "ores/cave_ceiling.png"
  terminations = ["events/the_wall_breaks.png", "events/can_not_reach_ceiling.png"]
  events = Region(798,686,222,73)
  
  def __init__(self):
    wurm.Bot.__init__(self)
    self.termination = 0
    wurm.switchTo()
    wait(2)
    self.pickaxe = wurm.toolbelt().find(Pattern("tools/active_pickaxe.png").similar(0.88))
    self.get_mine_type()
    if SKILLING:
      self.mine_cycles = SKILLING_WALL_CYCLES
    else:
      self.auto_sleep = False
  
  def repair_pickaxe(self):
    where2mine = Env.getMouseLocation() # Updates every time location so I can adjust position
    wurm.repair(self.pickaxe)
    wait(random.uniform(0.2, 1))
    hover(where2mine) 
  
  def get_mine_type(self):
    cursor = Env.getMouseLocation()
    ore = Region(cursor.x, cursor.y + 14, 94, 25)
    if SEE_CEILING and ore.exists(self.ceiling, 1):
      self.termination = self.terminations[1]
    else:
      self.termination = self.terminations[0]
  
  def do(self):
    for cycle in range(self.mine_cycles):
      type(config.KEY_MINE)
      wait(random.uniform(0.2, 1))  
    wait(10)
  
  def after_do(self):
    self.repair_pickaxe()
  
  def is_done(self):
    if self.termination: 
      return self.events.exists(self.termination, 0.8)
    return False 

wurm.execute(Miner())
