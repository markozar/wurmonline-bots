# After wurm focuses, you have 2s to put the cursor near the base
# of the tile to level down (this bot does not work to level up).
import config
reload(config)
import wurm
import random
reload(wurm)

# CUSTOMS ================================#
ACTION_TIME = 5   # level action timer
INV_DIRTS = 16        # How many dirts to carry before moving them to the storage
MAX_DROPS = 3
FAST_ACTION = 15  # if action requir
TOOLBELT_POS = 4  # position of the shovel in the toolbelt
#=========================================#

class LevelUpBot(wurm.Bot):
  inventory = wurm.inventory()
  inventory_bag = find("locations/inventory_group.png")

  def __init__(self):
    wurm.Bot.__init__(self)
    self.craft_time_margin = False
    self.auto_sleep = False
    
    wurm.switchTo()
    wait(2)
    self.level_here = Env.getMouseLocation()
    self.storage = selectRegion("Select dirt storage")
    wurm.switchTo()
    self.craft_bar = Region(942,769,129,23)
    self.stop = False
    self.take_timer = (INV_DIRTS / MAX_DROPS * ACTION_TIME) - ACTION_TIME;
    self.take_dirt()
   
  def take_dirt(self):
    dirt = self.storage.exists(Pattern("resources/pile_of_dirt.png").similar(0.9))
    if(not dirt):
      self.stop = True
      return
    dragDrop(dirt, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_max_items()

  def repair_shovel(self):
    wurm.repair(wurm.toolbelt(TOOLBELT_POS))

  def do(self):
    hover(self.level_here)
    wait(0.2)
    type(config.KEY_LEVEL)
    start = time.clock()
    last = start
    wait(2)
    self.level_here = Env.getMouseLocation() # Allow to correct position
    while self.craft_bar.exists(Pattern("patterns/leveling_action.png")):
      elapsed = time.clock() - last
      trigger = elapsed % (self.take_timer)
      if trigger < (elapsed-1):
        last += self.take_timer
        self.take_dirt()
      wait(1)
    self.take_dirt() 
    if (time.clock() - start) < FAST_ACTION:
      self.repair_shovel()
      self.stop = True
      print "Stopping"
        
  def is_done(self):
    return self.stop
  
wurm.execute(LevelUpBot())
