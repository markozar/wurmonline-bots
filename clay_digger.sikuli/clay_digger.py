import config
import wurm
import random

REPAIR_EVERY = 20
MAX_CLAY_IN_INVENTORY = 30

clay_digged = 0
clay_in_inventory = 0
clay_to_dig = 0

inventory = wurm.inventory()
shovel = wurm.toolbelt().find("tools/active_shovel.png")
storages = []
current_storage = 0
capacity_left = 0

def done():
  wurm.repair(shovel)
  popup("Done!")
  exit()

def get_parameters():
  global clay_to_dig, storages, capacity_left
  containers = int(input("How many containers?"))
  if containers == 0 or containers > 9: exit()
  for n in range(containers):
    wurm.switchTo()
    storage = {}
    storage['area'] = selectRegion("Storage " + str(n))
    storage['cap'] = int(input("Storage capacity?"))
    storages.append(storage)
    clay_to_dig += storage['cap']
  capacity_left = storages[current_storage]['cap']

def dig_cycle(n):
  global clay_digged, clay_in_inventory
  for cycle in range(n):
    type(config.KEY_DIG)
    clay_digged += 1
    clay_in_inventory += 1
    if(clay_in_inventory >= capacity_left): break
    wait(random.uniform(0.2, 1))

def repair_shovel():
  if clay_digged % REPAIR_EVERY == 0:
    wurm.repair(shovel)
    wait(random.uniform(0.2, 1))

def change_storage():
  global current_storage, capacity_left
  current_storage += 1
  if(current_storage >= len(storages)): done() # all filled up
  capacity_left = storages[current_storage]['cap']

def move_clay_to_storage():
  global clay_in_inventory, capacity_left
  if(clay_in_inventory >= MAX_CLAY_IN_INVENTORY or clay_in_inventory >= capacity_left):
    clay = inventory.find("resources/clay.png")
    dragDrop(clay, storages[current_storage]['area'])
    capacity_left -= clay_in_inventory
    clay_in_inventory = 0
    if(capacity_left <= 0): change_storage()
      
get_parameters()
wurm.switchTo()

while 1:
  dig_cycle(config.QUEUE_DEPTH)
  wait(15)
  wurm.wait_full_stamina()
  repair_shovel()
  move_clay_to_storage()
  wurm.restoration()
  