# put put a clay and sand in crafting slots and select mortar
# remove sand and clay from inventory
# start the bot
import config
reload(config)
import wurm
import random
reload(wurm)

CAN_CARRY = 2  # number of sands

wurm.switchTo()

class MortarMixerBot(wurm.Bot):
  inventory = wurm.inventory()
  inventory_bag = find("locations/inventory_group.png")
  crafting_slots = findAll("locations/empty_right_craft_slot.png")

  def __init__(self):
    wurm.Bot.__init__(self)
    #self.craft_time_margin = False
    
    self.slots = [] 
    while self.crafting_slots.hasNext():  
      self.slots.append(self.crafting_slots.next())
    if not len(self.slots) == 2:
      print "Open crafting window and empty crafting slots."
      exit()

    self.storage = selectRegion("Select sand and clay storage area")
    self.sands = int(input("How much sand to use?"))
    if self.sands <= 0: exit()

    wurm.switchTo()
    self.combine_s0 = self.slots[0].find("buttons/combine.png")
    self.combine_s1 = self.slots[1].find("buttons/combine.png")    
    self.take_more = True
   
  def take_mats(self):
    storage_sand = self.storage.exists("resources/sand.png")
    storage_clay = self.storage.exists("resources/clay.png")

    # takes sand and clay from storage 
    dragDrop(storage_clay, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_n_items(min(CAN_CARRY, self.sands) * 10)
    
    dragDrop(storage_sand, self.inventory_bag)
    wait(random.uniform(0.2, 0.4))
    wurm.select_n_items(min(CAN_CARRY, self.sands))

    wait(1)
    
    # add them to crafting window
    inventory_sand = self.inventory.exists(Pattern("resources/sand.png").similar(0.80))
    inventory_clay = self.inventory.exists(Pattern("resources/clay.png").similar(0.80))

    dragDrop(inventory_sand, self.slots[0])
    dragDrop(inventory_clay, self.slots[1])
    wait(1)
    click(self.combine_s0)
    wait(random.uniform(0.2, 0.4))
    click(self.combine_s1)
    self.take_more = False
    wait(1)

  def move_mortar_to_storage(self):
    mortar = self.inventory.exists("resources/mortar.png")
    if mortar: dragDrop(mortar, self.storage)

  def before_do(self):
   if self.take_more: self.take_mats()   
  
  def do(self):   
    wurm.click_create()
    wait(10)
  
  def is_done(self):
    return self.sands <= 0

  def too_fast(self):
    self.move_mortar_to_storage()
    self.sands -= CAN_CARRY
    if(self.sands > 0): self.take_more = True
    return False  # True => exit bot, False => continue botting

  def done(self):
    wurm.Bot.done(self)

wurm.execute(MortarMixerBot())
